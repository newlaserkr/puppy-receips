PROYECTO PARA BUSQUEDA DE RECETAS.

Proyecto realizado en kotlin y los casos de test realizados en Java.

##### DESCRIPCION PROYECTO
 
 El proyecto realiza busqueda de recetas cada 3 palabras o al hacer submit. Asimismo podemos 
 marcar una recta como favorito
 
 Eventos de la pantalla principal
 
 1) ActionBar buscar en la toolbar.
 2) ActionBar ir a Favoritas => para acceder a la pantalla de favoritas persistidas en la app.
 2) Boton para marcar como receta favorita.
 3) Item click para acceder al webview de la receta dentro de la misma app.
 
 ##### PENDIENTE IMPLEMENTACIÓN
 
 Entre otras mejoras, resta pendiente:
 1) Implementar la logica cuando no hay red.
 2) Refinamiento sistema de progress bar y caso de errores (ahora solo muestra un SnackBar)
 3) Estado de datos cuando se rota la panalla.
 4) Implementación de los bonus de la prueba.

##### LIBRERIAS PRINCIPALES DE TERCEROS

| LIBRERIA | DESCRIPCION |
| ------ | ------ |
| **SimpleSearchView** | Libreria para tener encapsulado con animación el componente SearchView. Evitamos la implementación en la activity principal |
| **Retrofit** | Utilizada para la comunicación con la api. Librería standar para las APIS | 
| **koin** | Inyector de dependencias en kotlin. No es mejor que Dagger pero para proyectos pequeños es muy simple de utilizar. Para formar juniors utlizar koin antes de Dagger, despues introducir a los components y subcomponents de Dagger |
| **room persistent** | Para la persistencia en base de datos. Existe la librería de Realm pero ya que hago una prueba quería probar el nuevo artefacto de mapeo de Google |
| **RxJava2** | Librería indiscutible para trabajar en background y de forma reactiva en las app. Con kotlin deja de ser tan necesario y se podría sustituir por las Corrutinas. |

